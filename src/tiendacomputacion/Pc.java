package tiendacomputacion;

import java.util.Date;

public abstract class Pc extends Producto{
    
    public Pc(String nombre, float precio, int cantidad,Date fechaRecibido) {
        super(nombre, precio, cantidad,fechaRecibido);
    }
    
}
