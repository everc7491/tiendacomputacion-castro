package tiendacomputacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreciosModelo implements Serializable{
    private List<Modelo> modeloLaptop;
    public PreciosModelo(){
        modeloLaptop=new ArrayList<>();
       
    }

    String obternerListadoModelos(){
        String lista="[0]-Agregar Modelo\n";
        if(modeloLaptop!=null){
            for(int i=0;i<modeloLaptop.size();i++){
              lista+="["+i+1+"]"+modeloLaptop.get(i).getTipoAdicional()+"\n";
            }
            
        }
        return lista;
    }
    Modelo obtenerModelo(int i){
        Modelo m=null;
        if(modeloLaptop!=null){
        m=modeloLaptop.get(i);
        }
        return m;
    }
    void AgregarModelo(String mod, float pre){
        Modelo m=new Modelo(mod,pre);
        modeloLaptop.add(m);
    }
    public List<Modelo> getModeloLaptop() {
        return modeloLaptop;
    }

    public void setModeloLaptop(List<Modelo> modeloLaptop) {
        this.modeloLaptop = modeloLaptop;
    }

}
