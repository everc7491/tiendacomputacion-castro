package tiendacomputacion;

import java.util.Date;

public class Periferico extends Producto{
    private String tipoPeriferico;
    private String descripcion;
    public Periferico(String descripcion,String tipoPeriferico, String nombre, float precio, int cantidad,Date fechaRecibido) {
        super(nombre, precio, cantidad,fechaRecibido);
        this.tipoPeriferico = tipoPeriferico;
        this.descripcion= descripcion;

        setTipo("PERIFERICO");
    }

    
    
    @Override
    public String mostrar() {
           float des;
      if(getCantidad()<10){
           des=(float) 0.20;
      }else{
          des=0;
      }
       return ""+getNombre()+" "+getDescripcion()+" $"+(getPrecio()-getPrecio()*des)+" cant:"+getCantidad();
    }

    public String getTipoPeriferico() {
        return tipoPeriferico;
    }

    public void setTipoPeriferico(String tipoPeriferico) {
        this.tipoPeriferico = tipoPeriferico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
