
package tiendacomputacion;
public abstract class AdicionalLaptop {
    private String tipoAdicional;
    private float adicional;

    public AdicionalLaptop(String tipoAdicional, float adicional) {
        this.tipoAdicional = tipoAdicional;
        this.adicional = adicional;
    }
 
    public String getTipoAdicional() {
        return tipoAdicional;
    }

    public void setTipoAdicional(String tipoAdicional) {
        this.tipoAdicional = tipoAdicional;
    }

    public float getAdicional() {
        return adicional;
    }

    public void setAdicional(float adicional) {
        this.adicional = adicional;
    }
}
