package tiendacomputacion;

import java.util.Date;

public class Almacenamiento extends Componente{
    private String tipoDisco;

    public Almacenamiento(String tipoDisco, String compatibilidad, float capacidad, String fabricante, String nombre, float precio, int cantidad,Date fechaRecibido) {
        super(compatibilidad, capacidad, fabricante, nombre, precio, cantidad,fechaRecibido);
        this.tipoDisco = tipoDisco;
        setComponente("ALMACENAMIENTO");
    }
   

    public String getTipoDisco() {
        return tipoDisco;
    }

    public void setTipoDisco(String tipoDisco) {
        this.tipoDisco = tipoDisco;
    }

    @Override
    public String mostrar() {
        float des;
        Date hoy=new Date(),com;
        com=getFechaRecibido();
        int a=com.getYear()+1;
        com.setYear(a);
      if(getCantidad()<10||(hoy.compareTo(com)<0)){
           des=(float) 0.20;
      }else{
          des=0;
            
      }
        return getNombre()+" "+tipoDisco+" "+getCapacidad()+" "+getFabricante()+" "+getCompatibilidad()+" $"+(getPrecio()-getPrecio()*des)+" cant:"+getCantidad(); 
    }
}
