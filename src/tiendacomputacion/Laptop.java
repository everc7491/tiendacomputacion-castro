package tiendacomputacion;

import java.util.Date;

public class Laptop extends Pc{
  
    public Laptop(Procesador pro, RAM ram, Almacenamiento alm, PlacaDeVideo pla, float pantalla, Marca marca, String nombre, float precio, int cantidad,Date fechaRecibido,Modelo modelo) {
        super(nombre, precio, cantidad, fechaRecibido);
        this.pro = pro;
        this.ram = ram;
        this.alm = alm;
        this.pla = pla;
        this.pantalla = pantalla;
        this.marca = marca;
        this.modelo=modelo;
         setTipo("LAPTOP");
    }
     private Procesador pro;
    private RAM ram;
    private Almacenamiento alm;
    private PlacaDeVideo pla;
    private float pantalla;
    private Marca marca;
    private Modelo modelo;

    public Procesador getPro() {
        return pro;
    }

    public void setPro(Procesador pro) {
        this.pro = pro;
    }

    public RAM getRam() {
        return ram;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public Almacenamiento getAlm() {
        return alm;
    }

    public void setAlm(Almacenamiento alm) {
        this.alm = alm;
    }

    public PlacaDeVideo getPla() {
        return pla;
    }

    public void setPla(PlacaDeVideo pla) {
        this.pla = pla;
    }

    public float getPantalla() {
        return pantalla;
    }

    public void setPantalla(float pantalla) {
        this.pantalla = pantalla;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    @Override
    public String mostrar() { 
        float pPlaca=0;
        String placa="-";
        if(pla!=null){
             pPlaca = pla.getPrecio();
             placa=pla.mostrar();
        }
        float preci=pro.getPrecio()+ram.getPrecio()+alm.getPrecio()+pPlaca;
        setPrecio(preci+=((pro.getPrecio()+ram.getPrecio()+alm.getPrecio()+pPlaca)*(modelo.getAdicional()+marca.getAdicional())/100));
       return "DETALLE de Laptop "+getNombre()+"\n\nMarca:"+getMarca()+"    Modelo:"+getModelo()+"\nProcesador:"+pro.mostrar()+"\nRAM:"+ram.mostrar()+"\nDisco:"+alm.mostrar()+"\nPlaca de Video:"+placa+"\nPantalla:"+getPantalla()+"\n$"+getPrecio();
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }
   
    
}
