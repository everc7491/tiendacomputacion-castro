package tiendacomputacion;

import java.io.Serializable;
import java.util.Date;

public class Desktop extends Pc implements Serializable{
    
   
    private Procesador pro;
    private RAM ram;

    public Desktop(Procesador pro, RAM ram, Almacenamiento alm, PlacaDeVideo pla, String nombre, float precio, int cantidad, Date fechaRecibido) {
        super(nombre, precio, cantidad, fechaRecibido);
        this.pro = pro;
        this.ram = ram;
        this.alm = alm;
        this.pla = pla;
         setTipo("DESKTOP");
        
    }
    private Almacenamiento alm;
    private PlacaDeVideo pla;

    public Procesador getPro() {
        return pro;
    }

    public void setPro(Procesador pro) {
        this.pro = pro;
    }

    public RAM getRam() {
        return ram;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public Almacenamiento getAlm() {
        return alm;
    }

    public void setAlm(Almacenamiento alm) {
        this.alm = alm;
    }

    public PlacaDeVideo getPla() {
        return pla;
    }

    public void setPla(PlacaDeVideo pla) {
        this.pla = pla;
    }

    @Override
    public String mostrar() {
        float pPlaca=0;
        String placa="-";
        if(pla!=null){
             pPlaca = pla.getPrecio();
             placa=pla.mostrar();
        }
        float preci=pro.getPrecio()+ram.getPrecio()+alm.getPrecio()+pPlaca;
        setPrecio(preci-=((pro.getPrecio()+ram.getPrecio()+alm.getPrecio()+pPlaca)*(20))/100);
       return "DETALLE del DESKTOP "+getNombre()+"\n\n"+"Procesador:"+pro.mostrar()+"\nRAM:"+ram.mostrar()+"\nDisco:"+alm.mostrar()+"\nPlaca de Video:"+placa+"\n$"+getPrecio();
      
    }
    
}
