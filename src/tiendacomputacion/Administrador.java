package tiendacomputacion;

import java.util.Date;

public class Administrador {
    private String usuario;
    private final String password;
    private Date hoy;
    private PreciosMarca pMarca;
    private PreciosModelo pModelo;
    private StockPc spc;
    private StockComponente sComp;
    private StockPeriferico sPeri;
    
    public Administrador(String usuario, String password,Date hoy) {
        this.usuario = usuario;
        this.password = password;
        this.hoy= hoy;
    }

    public void setSpc(StockPc spc) {
        this.spc = spc;
    }

    public void setSComp(StockComponente sComp) {
        this.sComp = sComp;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    boolean proceder() {
        int op,i,cant ;
        boolean seguir=true;
        String opcion;
     while(seguir==true){   
          do{
            op=EntradaSalida.leerInt("Menu Administrador\n\n"
              + "[0]CERRAR PROGRAMA\n"
              + "[1]stock COMPONENTES\n"
              + "[2]stock COMPUTADORAS\n"
              + "[3]Stock PERIFERICO \n"
              + "[4]alta COMPONENTE\n"
              + "[5]alta COMPUTADORA\n"
              + "[6]alta PRERIFERICO\n" 
              + "[7]vender COMPONENTE\n"
              + "[8]vender COMPUTADORA\n"
              + "[9]vender PERIFERICO");
    }while(op<0 || op>9);
        switch(op){
            case 0:
           seguir= false;     
              break;
            case 1:
                EntradaSalida.mostrarString(sComp.listadoComponentes());
                break;
            case 2:
                EntradaSalida.mostrarString(spc.listadoPc());
                break;
            case 3:
                EntradaSalida.mostrarString(sPeri.listadoPeriferico());
                break;
            case 4:
               altaPeriferico(hoy);
                break;
            case 5:
                EntradaSalida.mostrarString("Armando pc!");
                opcion=EntradaSalida.leerString("[0]-LAPTOP\n"
                                      + "[otra tecla]-DESKTOP");
              if(opcion.equals('0')){
                  opcion="LAPTOP";
              }else{
                  opcion="DESKTOP";
              }
            
                 if( verificarComponentes(opcion)){
               
                  cargarPC(opcion);
                 }
                break;
            case 6:
              altaPeriferico(hoy);
                break;
            case 7: 
            if(sComp.existenComponentes()){
                     EntradaSalida.mostrarString("VENTA");
                     i=EntradaSalida.leerInt("Stock de componentes \n\n"
                          + sComp.verListadoComponente("TODO", "TODO"));
                     
                     cant=EntradaSalida.leerInt("Cantidad a vender:");
                     if(codigoCorrectoComp(i)){
                     sComp.saleComponente(i,cant);
                     }
            }else{
                EntradaSalida.mostrarString("No hay Componentes");
            }
                break;
            case 8:
                if(spc.existenPc()){
                    EntradaSalida.mostrarString("VENTA");
                    i=EntradaSalida.leerInt("Stock de computadoras \n\n"
                        + spc.listadoPc());
                    cant=EntradaSalida.leerInt("Cantidad a vender:");
                    if(codigoCorrectoPc(i)){
                    spc.venderComputadora(i-1,cant);
                    }
                }else{
                    EntradaSalida.mostrarString("No hay PC cargadas");
                }
                break;
            case 9:
                if(sPeri.existePerifericos()){ 
                     EntradaSalida.mostrarString("Venta");
                     i=EntradaSalida.leerInt("Stock de Perifericos\n\n"
                            +sPeri.listadoPeriferico());
                            cant=EntradaSalida.leerInt("Cantidad a vender");
                       if( codigoCorrecto(i)){
                            sPeri.venderPeriferico(i-1, cant);
                       }
                            
                }else{
                    EntradaSalida.mostrarString("NO hay perifericos cargados");
                }
                break;
        }  
        }
        return seguir;
        }    
  String marca(){
        return EntradaSalida.leerString("Marca:");
    }
    
    String nombre(){
        return EntradaSalida.leerString("Nombre:");
    }
    float capacidad(){
        return EntradaSalida.leerFloat("Capacidad:");
    }
    float precio(){
        return EntradaSalida.leerFloat("Precio $");
    }
    int cantidad(){
        return EntradaSalida.leerInt("Cantidad:");
    }
    String compatibilidad(){
        int op;
        String comp="";
        do{
             op=EntradaSalida.leerInt("¿Que Compatibilidad tiene?\n\n"
                + "[1]-Escritorio\n"
                + "[2]-Portatil\n"
                + "[3]-Ambas");
        }while(op<1||op>3);
        switch(op){
            case 1:
                comp= "DESKTOP";
                break;
            case 2:
                comp= "LAPTOP";
                break;
            case 3:
                comp= "AMBAS";
                break;
        }
        return comp;
    }
 void cargarPC(String compatibilidad){
 
    int i;
    boolean ca;
    String nombre="";
    float tam=0;
    Marca marc=null;
    Modelo mo=null;
       nombre=EntradaSalida.leerString("Nombre del equipo "+compatibilidad+":");
    
    if(compatibilidad.equals("LAPTOP")){
         marc=marcaLaptop();
         mo=modeloLaptop();    
        tam=EntradaSalida.leerFloat("Tamaño de la pantalla");
    }  
                i=EntradaSalida.leerInt("Elegir Procesador:\n\n"+sComp.verListadoComponente("PROCESADOR",compatibilidad));
                Procesador p=sComp.obtenerProcesador(i);
                i=EntradaSalida.leerInt("Elegir Memoria Ram:\n\n"+sComp.verListadoComponente("RAM",compatibilidad));
                RAM r=sComp.obtenerRAM(i);
                i=EntradaSalida.leerInt("Elegir Disco de almacenamiento:\n\n"+sComp.verListadoComponente("ALMACENAMIENTO",compatibilidad));
                Almacenamiento a=sComp.obtenerAlmacenamiento(i);
              
                ca=EntradaSalida.leerBoolean("¿Agregar Placa de Video?");
               
                PlacaDeVideo pl=null;
                
                if(ca){
                    if(sComp.existePlaca(compatibilidad)){
                    i=EntradaSalida.leerInt("Elegir Placa de Video:\n\n"+sComp.verListadoComponente("PLACA_VIDEO", compatibilidad));
                    pl=sComp.obtenerPlaca(i);
                    }else{
                        EntradaSalida.mostrarString("NO se encuentran placa de video compatible");
                    }
                }
         if(compatibilidad.equals("LAPTOP")){
            
                      Laptop lap=new Laptop(p,r,a,pl,tam,marc,nombre,0,1,hoy,mo);
                      spc.agregarPC(lap);
         }   else{
             Desktop desk=new Desktop(p,r,a,pl,nombre,0,1,hoy);
             spc.agregarPC(desk);
         }
     
}
Marca marcaLaptop(){
    Marca m=null;
    int i;
          do{
        i=EntradaSalida.leerInt("Elegir marca:\n"+pMarca.obternerListadoMarcas());
        } while(i<0||i>pMarca.cantidadMarca()+1);
       
        if(i==0){
           String ma=EntradaSalida.leerString("Ingrese marca nueva:");
           float p=EntradaSalida.leerFloat("Porcentaje de aumento");
           m=new Marca(ma,p);
        }else{
            m=pMarca.obtenerMarca(i-1);
        }
    return m;
}
    public void setsComp(StockComponente sComp) {
        this.sComp = sComp;
    }

    public void setsPeri(StockPeriferico sPeri) {
        this.sPeri = sPeri;
    }

    private void cargarPeriferico(String periferico,Date fecha) {
        String descripcion=EntradaSalida.leerString("Descripcion");
                        Periferico peri=new Periferico(descripcion,periferico,nombre(),precio(),cantidad(),fecha);
                        sPeri.agregarPeriferico(peri);
    }
    boolean verificarComponentes(String compatibilidad){
        boolean a;
        String falta="";
        a=sComp.hayComponenete("PROCESADOR", compatibilidad);
        if(a==false){
            falta+="-Procesador";
        }
        a=sComp.hayComponenete("RAM", compatibilidad);
        if(a==false){
            falta+="-Ram";
        }
        a=sComp.hayComponenete("ALMACENAMIENTO", compatibilidad);
        if(a==false){
            falta+="-Almacenamiento";
        }
        if(falta.equals("")){
            EntradaSalida.mostrarString("Verificacion de Componentes exitosa");
            a=true;
        }else{
            EntradaSalida.mostrarString("Componentes Insuficientes\nFalta "+falta);
            a=false;
        }
        return a;
    }
    void altaPeriferico(Date fecha){
        int ca;
          EntradaSalida.mostrarString("Alta PERIFERICO:");
                do{
                ca=EntradaSalida.leerInt("Tipo de Periferico\n\n"
                        + "[0]-CANCELAR\n"
                        + "[1]-Teclado\n"
                        + "[2]-Web Cam\n"
                        + "[3]-Mouse\n"
                        + "[4]-Monitor");
                }while(ca<0||ca>4);
                switch(ca){
                    case 0:  
                      
                        break;
                    case 1:
                        cargarPeriferico("TECLADO",fecha);
                        break;
                    case 2:
                        cargarPeriferico("WEBCAM",fecha);
                        break;
                    case 3:
                         cargarPeriferico("MOUSE",fecha);
                        break;
                    case 4:
                        cargarPeriferico("MONITOR",fecha);
                        
                }
    }
    void altaComponente(Date fecha){
        int o;
         do{
                o=EntradaSalida.leerInt("ALTA DE COMPONENTES\n\n"
                        + "[0]VOLVER\n"
                        + "[1] Procesador\n"
                        + "[2] Ram\n"
                        + "[3] Almacenamiento\n"
                        + "[4] Placa de Video");
                }while(o<0||o>4);
                   switch(o){
                       case 0:
                           break;
                       case 1:
                           Procesador p=altaProce(fecha);
                            sComp.agregarProcesador(p);
                           break;
                       case 2:
                           RAM r=altaRAM(fecha);
                           sComp.agregarRam(r);
                           break;
                       case 3:
                       Almacenamiento a=altaAlmacenamiento(fecha);
                           sComp.agregarAlmacenamiento(a);
                          
                           break;
                       case 4:
                          PlacaDeVideo pl=altaPlaca(fecha);
                           sComp.agregarPlaca(pl);
                           break;
                            }
    }
public void primerArranque(){
    int i;
    EntradaSalida.mostrarString("A continuacion daremos de alta el stock ya equistente:");
    do{
       do{
       i=EntradaSalida.leerInt("MENU De primer arranque:"
               + "\n\n[0]-Salir"
               + "\n[1]-alta Periferico"
               + "\n[2]-alta Componente"
               + "\n[3]-alta PC");
       }while(i<0||i>3);
       switch(i){
           case 0:
               if(EntradaSalida.leerBoolean("¿Esta seguro que no va a cargar mas Stock?")){
                   i=5;
               }
               break;
           case 1:
               altaPeriferico(fechaIngreso());
               break;
           case 2:
               altaComponente(fechaIngreso());
               break;
           case 3:
             
            EntradaSalida.mostrarString("Nueva PC");
             String nombre=EntradaSalida.leerString("Nombre del equipo:");
             EntradaSalida.mostrarString("Especificaciones del procesador");
            Procesador p=altaProce(fechaIngreso());
             EntradaSalida.mostrarString("Especificaciones de la memoria RAM");
            RAM r=altaRAM(fechaIngreso());
             EntradaSalida.mostrarString("Especificaciones del dispositivo de almacenamiento");
            Almacenamiento al=altaAlmacenamiento(fechaIngreso());
            PlacaDeVideo pl=null;
            if(EntradaSalida.leerBoolean("¿Colocar Placa de Video?")){
                 EntradaSalida.mostrarString("Especificaciones de la placa de video");
                 pl=altaPlaca(fechaIngreso());
            }
             if(EntradaSalida.leerBoolean("[Si]-LAPTOP\n[No]-DESKTOP")){
             
                 float pant=EntradaSalida.leerFloat("Especifique tamaño de la pantalla");
                 Marca mar=marcaLaptop();
                 Modelo mod=modeloLaptop();
                 Laptop l=new Laptop(p,r,al,pl,pant,mar,nombre,0,1,fechaIngreso(), mod);
                 spc.agregarPC(l);
             }else{
                 Desktop d=new Desktop(p,r,al,pl,nombre,0,1,fechaIngreso());
                 spc.agregarPC(d);
             }
       }       
    }while(i!=0);
}
    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }

    public PreciosMarca getpMarca() {
        return pMarca;
    }

    public void setpMarca(PreciosMarca pMarca) {
        this.pMarca = pMarca;
    }

    public PreciosModelo getpModelo() {
        return pModelo;
    }

    public void setpModelo(PreciosModelo pModelo) {
        this.pModelo = pModelo;
    }

    private Modelo modeloLaptop() {
         Modelo m=null;
    int i;
          do{
        i=EntradaSalida.leerInt("Elegir marca:\n"+pModelo.obternerListadoModelos());
        } while(i<0||i>pMarca.cantidadMarca()+1);
       
        if(i==0){
           String ma=EntradaSalida.leerString("Ingrese marca nueva:");
           float p=EntradaSalida.leerFloat("Porcentaje de aumento");
           m=new Modelo(ma,p);
        }else{
            m=pModelo.obtenerModelo(i-1);
        }
    return m;
    }

    private Procesador altaProce(Date fecha) {
        String aux=EntradaSalida.leerString("Modelo:");
        Procesador p=new Procesador(aux,compatibilidad(),capacidad(),marca(),nombre(),precio(),cantidad(),fecha);  
        return p;
    }

    private RAM altaRAM(Date fecha) {
       String aux=EntradaSalida.leerString("Tecnologia:");
     float frecuencia=EntradaSalida.leerFloat("Frecuencia:");
     RAM r=new RAM(aux,frecuencia,compatibilidad(),capacidad(),marca(),nombre(),precio(),cantidad(),fecha);
    return r;
    }

    private Almacenamiento altaAlmacenamiento(Date fecha) {
        String aux="";
        if(EntradaSalida.leerBoolean("Elija tipo de Almacenamientos:\n\n"
                                  + "[SI]-SSD\n"
                                  + "[NO]-HHD")){
              aux="SSD";
        }else{
              aux="HHD";
             }
         Almacenamiento a=new Almacenamiento(aux,compatibilidad(),capacidad(),marca(),nombre(),precio(),cantidad(),fecha);
         return a;
    }

    private PlacaDeVideo altaPlaca(Date fecha) {
      String aux=EntradaSalida.leerString("Modelo:");
      PlacaDeVideo pl=new PlacaDeVideo(aux,compatibilidad(),capacidad(),marca(),nombre(),precio(),cantidad(),fecha);
      return pl;
    }

   Date fechaIngreso(){
       Date d=new Date(EntradaSalida.leerInt("Ingresar año que ingreso(yyyy)"),EntradaSalida.leerInt("Ingrese mes de Ingreso(MM)"),EntradaSalida.leerInt("Ingrese dia de ingreso(DD)"));
       return d;
   }

    private boolean codigoCorrecto(int buscar) {
        
    boolean r=false;
    int aux;
 
        for( int i=0;i<sPeri.obtenerCodDisp().size();i++){
            aux=(int) sPeri.obtenerCodDisp().get(i);
          if(buscar==aux){
              r=true;
          }
        }
        return r;
    }

    private boolean codigoCorrectoPc(int buscar) {
        
    boolean r=false;
    int aux;
 
        for( int i=0;i<spc.obtenerCodDisp().size();i++){
            aux=(int) spc.obtenerCodDisp().get(i);
          if(buscar==aux){
              r=true;
          }
        }
        return r;
    }

    private boolean codigoCorrectoComp(int buscar) {
         boolean r=false;
    int aux;
 
        for( int i=0;i<sComp.obtenerCodDisp().size();i++){
            aux=(int) sComp.obtenerCodDisp().get(i);
          if(buscar==aux){
              r=true;
          }
        }
        return r;
    }
   
}
