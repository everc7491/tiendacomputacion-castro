package tiendacomputacion;

import java.util.Date;

public class RAM extends Componente{
    
    private String tecnologia;
    private float frecuencia;

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }

    public float getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(float frecuencia) {
        this.frecuencia = frecuencia;
    }

    public RAM(String tecnologia, float frecuencia, String compatibilidad, float capacidad, String fabricante, String nombre, float precio, int cantidad, Date fechaRecibido) {
        super(compatibilidad, capacidad, fabricante, nombre, precio, cantidad, fechaRecibido);
        this.tecnologia = tecnologia;
        this.frecuencia = frecuencia;
         setComponente("RAM");
    }

    @Override
    public String mostrar() {
           float des;
      if(getCantidad()<10){
           des=(float) 0.20;
      }else{
          des=0;
      }
       return getNombre()+" "+getTecnologia()+" "+getCapacidad()+" GB"+getFrecuencia()+" Hz"+ getCompatibilidad()+" $"+(getPrecio()-getPrecio()*des)+" cant:"+getCantidad();
    }
 
    
    
   
}
