package tiendacomputacion;

import java.util.Date;

public abstract class Componente extends Producto{
    
    float capacidad;
    String fabricante;
    String compatibilidad;
    private String componente;
    
    public Componente(String compatibilidad,float capacidad, String fabricante, String nombre, float precio, int cantidad,Date fechaRecibido) {
        super(nombre, precio, cantidad,fechaRecibido);
        this.capacidad = capacidad;
        this.fabricante = fabricante;
        this.compatibilidad = compatibilidad;
               setTipo("COMPONENTE");
    }

  

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getCompatibilidad() {
        return compatibilidad;
    }

    public void setCompatibilidad(String compatibilidad) {
        this.compatibilidad = compatibilidad;
    }

    public String getComponente() {
        return componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }
}
