package tiendacomputacion;

import java.util.Date;

public class Procesador extends Componente{
    
   
    private String modelo;

    public Procesador(String modelo, String compatibilidad, float capacidad, String fabricante, String nombre, float precio, int cantidad,Date fechaRecibido) {
        super(compatibilidad, capacidad, fabricante, nombre, precio, cantidad,fechaRecibido);
        this.modelo = modelo;
         setComponente("PROCESADOR");
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public String mostrar() {
           float des;
      if(getCantidad()<10){
           des=(float) 0.20;
      }else{
          des=0;
      }
       return getNombre()+" "+getModelo()+" "+getFabricante()+" "+getCapacidad()+"Ghz "+getCompatibilidad()+" $"+(getPrecio()-getPrecio()*des)+" cant:"+getCantidad();
    }
  
    
}
