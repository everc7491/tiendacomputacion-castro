package tiendacomputacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StockComponente implements Serializable {
    private List<Componente> componentes;
    private Date hoy;
    
    public StockComponente(){
        componentes=new ArrayList<>();
    }

    public List<Componente> getComponentes() {
        return componentes;
    }

    public void setComponentes(List<Componente> componentes) {
        this.componentes = componentes;
    }

    String listadoComponentes() {
      String lista = "";
       Componente pro=null;
        if(componentes!=null){     
               for (int i = 0; i < componentes.size(); i++) {
               pro=componentes.get(i);
                   if((pro.getTipo()).equals("COMPONENTE")){
                      lista +=pro.mostrar()+
                              "\n";
                    }
                } 
        }else {
            
            EntradaSalida.mostrarString("No hay componentes cargados");
        }
        return lista;
    }
     String listadoPerifericos() {
      String lista = "\n";
       Componente pro=null;
        if(componentes!=null){     
               for (int i = 0; i < componentes.size(); i++) {
               pro=componentes.get(i);
                   if((pro.getTipo()).equals("PERIFERICO")){
                      lista +=pro.getNombre() +pro.getPrecio()+pro.getCantidad()+"\n";
                    }
                } 
        }
        return lista;
    }

    void agregarProcesador(Procesador p) {
        componentes.add(p);
    }

    void agregarRam(RAM r) {
        componentes.add(r);
    }

    void agregarAlmacenamiento(Almacenamiento a) {
      componentes.add(a);
    }

    void agregarPlaca(PlacaDeVideo pl) {
        componentes.add(pl);
    }

    String verListadoComponente(String componente,String compatibilidad) {
       String lista="";
        Componente pro=null;
        if(componentes!=null){
        for (int i = 0; i < componentes.size(); i++) {
            pro=componentes.get(i); 
            if(pro.getCompatibilidad().equals(compatibilidad)||pro.getCompatibilidad().equals("AMBOS")||compatibilidad.equals("TODO"))
            if((pro.getComponente().equals(componente))||componente.equals("TODO")){
           lista+="["+i+"]"+pro.mostrar()+"\n";
            }
        }
        }else{
            lista="NO hay Componentes Cargados!";
        }
       return lista;
    }

    Procesador obtenerProcesador(int j) {
        Procesador p=(Procesador) componentes.get(j);
        saleComponente(j,1);
        return p;
    }

    RAM obtenerRAM(int i) {
       RAM r=(RAM) componentes.get(i);
       saleComponente(i,1);
       return r;
    }

    Almacenamiento obtenerAlmacenamiento(int i) {
       Almacenamiento a=(Almacenamiento) componentes.get(i);
       saleComponente(i,1);
       return a;
    }

    PlacaDeVideo obtenerPlaca(int i) {
        PlacaDeVideo pl=(PlacaDeVideo) componentes.get(i);
        saleComponente(i,1);
        return pl;
    }

    void saleComponente(int i, int cant) {
       
        int cantidad=componentes.get(i).getCantidad()-cant;
        if(cantidad>=0){
       
        componentes.get(i).setCantidad(cantidad);
        }else{
            
            EntradaSalida.mostrarString("Cantidad de elementos a vender SUPERA el stock");
        }
        
    }
     boolean hayComponenete(String componente, String compatibilidad){
        boolean hay=false;
        for(Componente c :componentes){
            if(c.getComponente().equals(componente)&&c.getCantidad()>0){
                if(c.getCompatibilidad().equals(compatibilidad)||c.getCompatibilidad().equals("AMBAS")){
                   hay=true;
                   break;
                }
            }
        }
        
        return hay;
    }

    boolean existenComponentes() {
       if(componentes==null){
           return false;
       }else{
           return true;
       }
    }boolean existePlaca(String compatibilidad){
        boolean t=false; 
        if(componentes==null){
        EntradaSalida.mostrarString("NO hay componentes");
        } else{
           for(int i=0;i<=componentes.size();i++){
            if( componentes.get(i).getComponente().equals("PLACA_VIDEO")&&(componentes.get(i).getCompatibilidad().equals("compatibilidad")||componentes.get(i).getCompatibilidad().equals("AMBAS"))){
             t=true;
             break;
            }
           }
        }
        return t;
        }

    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }
    
 ArrayList obtenerCodDisp(){
        
    ArrayList<Integer> num=new ArrayList<Integer>();
    Componente p=null;
        if(componentes!=null){     
               for (int i = 0; i < componentes.size(); i++) {
               p=componentes.get(i);
                   if((p.getPrecio())!=0){
                     num.add(i);
                    }
                } 
        }
    return num;
    }

   
}
