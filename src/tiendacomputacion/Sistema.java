package tiendacomputacion;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Sistema implements Serializable{
    
    private StockComponente sComp;
    private StockPc spc;
    private List<Administrador> admi;
    private StockPeriferico sPeri;
    private Date hoy;
    public Sistema() {
        sComp = new StockComponente();
        spc = new StockPc();
        sPeri=new StockPeriferico();
        admi=new ArrayList<>();
        hoy= new Date();
    }

    public Sistema deSerializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(archivo);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        s.setHoy(hoy);
   
        return s;
    }

    public void serializar(String archivo) throws IOException {
        FileOutputStream f = new FileOutputStream(archivo);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
    }

    public void setSComp(StockComponente sComp) {
        this.sComp = sComp;
    }

    public void setSpc(StockPc spc) {
        this.spc = spc;
    }

    public StockComponente getSComp() {
        return sComp;
    }

    public StockPc getSpc() {
        return spc;
    }
    void agregarAdministrador(Administrador a){
       
    }
 public void arrancar() {
        boolean corriendo = true;

        while (corriendo) {
           

            String usuario = EntradaSalida.leerString("Ingrese su usuario");
            String password = EntradaSalida.leerPassword("Ingrese password");

           for( Administrador a :admi){
                if (a.getUsuario().equals(usuario) && a.getPassword().equals(password)) {
                    corriendo=a.proceder();
                }else{
                EntradaSalida.mostrarString("Usuario/contraseña inexistente");
                }
            }
        }
        EntradaSalida.mostrarString("Hasta mañana");
    }

    public void inicializacion() {
        String usuario, password;

        EntradaSalida.mostrarString(
                "PRIMER ARRANQUE\n\n"
                + "A continuación genere administrador para el sistema.");

        usuario = EntradaSalida.leerString("Nombre de usuario del Administrador.");
        password = EntradaSalida.leerPassword("Contraseña del Administrador.");
        Administrador a = new Administrador(usuario, password,hoy);
        
        a.setSpc(spc);
        a.setSComp(sComp);
        a.setsPeri(sPeri);
      
        admi.add(a);
       a.primerArranque();
        EntradaSalida.mostrarString( "ADMINISTRADOR CARGADO\n\n"
                + "Ya puede ingresar al sistema."  );
    }


    public void setsPeri(StockPeriferico sPeri) {
        this.sPeri = sPeri;
    }

    public StockComponente getsComp() {
        return sComp;
    }

    public void setsComp(StockComponente sComp) {
        this.sComp = sComp;
    }

    public List<Administrador> getAdmi() {
        return admi;
    }

    public void setAdmi(List<Administrador> admi) {
        this.admi = admi;
    }

    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }
}
