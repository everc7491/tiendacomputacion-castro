package tiendacomputacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StockPc implements Serializable{
    private List<Pc> pcs;
    public StockPc(){
        pcs=new ArrayList<>();
    }

    public List<Pc> getPcs() {
        return pcs;
    }

    public void setPcs(List<Pc> pcs) {
        this.pcs = pcs;
    }

    String listadoPc() {
       String lista = "";
       Pc p=null;
        if(pcs==null){     
             EntradaSalida.mostrarString("No hay computadoras para vender");
                } else{
                  for (int i = 0; i < pcs.size(); i++) {
               p=pcs.get(i);              
                      lista +="\n["+i+"]"+p.mostrar();        
                        }
        }
        return lista;
    }

    void agregarPC(Pc p) {
        if(p!=null){
        pcs.add(p);
        }
    }

    void venderComputadora(int i, int cant) {
        int cantidad=pcs.get(i).getCantidad();
      
        if(cantidad-cant>=0){
        pcs.get(i).setCantidad(cantidad);
        }else{
            EntradaSalida.mostrarString("La cantidad requerida supera el stock");
        }
        
    }

    boolean existenPc() {
        boolean v;
       if(pcs==null){
           v= false;
       }else{
           v= true;
       }
       return v;
       
    }

    
    ArrayList obtenerCodDisp(){
        
    ArrayList<Integer> num=new ArrayList<Integer>();
    Pc p=null;
        if(pcs!=null){     
               for (int i = 0; i < pcs.size(); i++) {
               p=pcs.get(i);
                   if((p.getPrecio())!=0){
                     num.add(i);
                    }
                } 
        }
    return num;
    
    }
}
