
package tiendacomputacion;

import java.io.Serializable;
import java.util.Date;

public abstract class Producto implements Serializable{
   private String  nombre;
   private String tipo;
   private float precio;
   private int cantidad;
   private Date fechaRecibido;

    public Producto(String nombre, float precio, int cantidad, Date fechaRecibido) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.fechaRecibido=fechaRecibido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public abstract String mostrar();

    public Date getFechaRecibido() {
        return fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }
}
