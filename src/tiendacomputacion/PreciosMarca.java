package tiendacomputacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreciosMarca implements Serializable{
    private List<Marca> marcaLaptop;
   
    public PreciosMarca(){
        marcaLaptop=new ArrayList<>();
       
    }

        String obternerListadoMarcas(){
        String lista="[0]-Agregar Modelo\n";
        if(marcaLaptop!=null){
            for(int i=0;i<marcaLaptop.size();i++){
              lista+="["+i+1+"]"+marcaLaptop.get(i).getTipoAdicional()+"\n";
            }
            
        }
        return lista;
    }
    void AgregarMarca(String mod, float pre){
        Marca m=new Marca(mod,pre);
        marcaLaptop.add(m);
       
    }
    Marca obtenerMarca(int i){
        Marca m=null;
        if(marcaLaptop!=null){
             m=marcaLaptop.get(i);
        }else{
            EntradaSalida.mostrarString("Ocurrio un error cargando las marcas");
        }
        return m;
    }
    public List<Marca> getMarcaLaptop() {
        return marcaLaptop;
    }

    public void setMarcaLaptop(List<Marca> marcaLaptop) {
        this.marcaLaptop = marcaLaptop;
    }

    int cantidadMarca() {
        return marcaLaptop.size();
    }

   
}
