package tiendacomputacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StockPeriferico implements Serializable{
    private List<Periferico> perifericos;
    public StockPeriferico(){
        perifericos= new ArrayList<>();
    }

    public List<Periferico> getPerifericos() {
        return perifericos;
    }

    public void setPerifericos(List<Periferico> perifericos) {
        this.perifericos = perifericos;
    }

    void agregarPeriferico(Periferico peri) {
        if(peri!=null){
        perifericos.add(peri);
        }
    }

    void venderPeriferico(int i, int cant) {
        if(perifericos.get(i)!=null){
         int cantidad=perifericos.get(i).getCantidad()-cant;
        if(cantidad>=0){
        perifericos.get(i).setCantidad(cantidad);
    }else {
            EntradaSalida.mostrarString("La cantidad requerida Supera el STOCK");
        }
        }
    }
    String listadoPeriferico() {
        String lista = "[0]-SALIR";
       Periferico p=null;
         if(perifericos==null){
              EntradaSalida.mostrarString("No hay perifericos CARGADOS");
         }else{
            for (int i = 0; i < perifericos.size(); i++) {
               p=perifericos.get(i);  
               if(p.getCantidad()>0){
                      lista +="\n["+(i+1)+"]"+p.mostrar();
               }
                }   
         }
        
        return lista;
    }

    boolean existePerifericos() {
        boolean v;
        if(perifericos!=null){
            v= true;
        }else{
            v=false;
        }
        return v;
    }

   
    ArrayList obtenerCodDisp(){
        
    ArrayList<Integer> num=new ArrayList<Integer>();
    Periferico p=null;
        if(perifericos!=null){     
               for (int i = 0; i < perifericos.size(); i++) {
               p=perifericos.get(i);
                   if((p.getPrecio())!=0){
                     num.add(i);
                    }
                } 
        }
    return num;
    }
    
    
}

